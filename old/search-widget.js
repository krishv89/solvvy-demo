var solvvy = (function () {
    var $;
    var jQueryScriptOutputted = false;

    var config = {
        jQueryUrl: "https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js",
        solvvyApiUrl: "https://www.solvvy.com/api",
        solvvyImagesUrl: "https://www.solvvy.com/search-widget",
//        mode: "in-place"
        mode: "float",
        inPlaceSelector: "#solvvy-search",
        numResults: 20,
        scrollForMoreResults: true,
        querySuggestions: true
    };

    var totalResults;
    var lastBatchCount;
    var protocol = (window.location.hostname.indexOf('https')) != -1 ? 'https://' : 'http://';

    function createSearchWidget(extraConfig) {
        initYouTubeAPI();
        initMixPanel(extraConfig);
        initJQuery(function (_$) {
            $ = _$;

            $.extend(config, extraConfig);

            if (config.mode == "in-place") {
                if ($(config.inPlaceSelector).size() == 0 && console && console.log) {
                    console.log("Solvvy WARNING: selector '" + config.inPlaceSelector + "' for in-place search widget does not match anything")
                    return;
                }

                if (!config.scrollContainer) {
                    config.scrollContainer = "window";
                }


                $(config.inPlaceSelector).empty().append(
                    '<div id="solvvy_inplace_widget" class="solvvy_widget">' +
                    '<form name="solvvy_inplace_search_form">' +
                    '<div class="solvvy_query_wrapper">' +
                    '<input type="text" name="solvvy_q" class="solvvy_query" placeholder="Ask anything here..." required />' +
                    '</div>' +
                    '<input type="submit" class="solvvy_search" value="Search" />' +
                    '</form>' +
                    '<div class="solvvy_results" style="display: none;"></div>' +
                    '<div class="solvvy_load_more" style="display: none;">' +
                    '<img style="width: 30px; height: 30px;" src="' + config.solvvyImagesUrl + '/searching.gif">' +
                    '</div>' +
                    '</div>'
                );
                initInPlaceSearchWidget();
            } else {

                if (!config.scrollContainer) {
                    config.scrollContainer = ".solvvy_results_wrapper";
                }

                $("body").append(
                     '<div id="solvvy_left">' +
                     '<img class="solvvy_widget_icon" id="solvvy_button" src="widget_icon.png" / >' +   
                    '<div id="solvvy_float_widget" class="solvvy_widget">' +
                    '<div id = "solvvy_header" class="solvvy_header"> Looking for something?</div>' + 
                    '<img id="left_arrow" src="left_arrow.png" / >'+
                    '<form name="solvvy_float_search_form">' + 
                    '<span id="search_solvvy"><img id="search_solvvy_img" src="search_icon.png"/></span>'+
                    '<input type="text" name="solvvy_q" id="solvvy_input_search" class="" placeholder="Ask anything here ..."/>' +
                    '<input type="reset" style="outline:none;display:none;" id="reset_search_box" value="" alt="clear" />'+
                    '<span id="solvvy_results_count"></span>' +
                    '</form>' +
                    '<div class="solvvy_results_wrapper " id="solvvy_scroller" style="display: none;">' + 
                    '<div id="most_popular">MOST POPULAR QUESTIONS</div>'+
                        '<div class="solvvy_results" style="display: none;"></div>' + 
                        '<div class="solvvy_load_more" style="display: none;">' +
                            '<img src="' + config.solvvyImagesUrl + '/searching.gif">' +
                        '</div>' +
                    '</div>' +
                    '<div class="solvvy_footer">' + 
                    '<span>&#64;</span>' +
                    '<span>'+' Contact us'+'</span>'+
                    '<span class="solvvy_powered-by">' +
                    'Powered by <span> Solvvy</span>' +
                    '</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' 
                );
                initFloatingSearchWidget();
            }

            $.fn.scrollTo = function(target, options, callback){
                if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
                var settings = $.extend({
                    scrollTarget  : target,
                    offsetTop     : 150,
                    duration      : 500,
                    easing        : 'swing'
                }, options);
                
                return this.each(function(){
                    var scrollPane = $(this);
                    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
                    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
                    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
                        if (typeof callback == 'function') { callback.call(this); }
                    });
                });
            }
        });
    };

    function initYouTubeAPI() {
        var tag = document.createElement('script');
        tag.src = protocol + "www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }

    function initMixPanel(config) {
        (function (f, b) {
            if (!b.__SV) {
                var a, e, i, g;
                window.mixpanel = b;
                b._i = [];
                b.init = function (a, e, d) {
                    function f(b, h) {
                        var a = h.split(".");
                        2 == a.length && (b = b[a[0]], h = a[1]);
                        b[h] = function () {
                            b.push([h].concat(Array.prototype.slice.call(arguments, 0)))
                        }
                    }

                    var c = b;
                    "undefined" !== typeof d ? c = b[d] = [] : d = "mixpanel";
                    c.people = c.people || [];
                    c.toString = function (b) {
                        var a = "mixpanel";
                        "mixpanel" !== d && (a += "." + d);
                        b || (a += " (stub)");
                        return a
                    };
                    c.people.toString = function () {
                        return c.toString(1) + ".people (stub)"
                    };
                    i = "disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
                    for (g = 0; g < i.length; g++)f(c, i[g]);
                    b._i.push([a, e, d])
                };
                b.__SV = 1.2;
                a = f.createElement("script");
                a.type = "text/javascript";
                a.async = !0;
                a.src = "undefined" !== typeof MIXPANEL_CUSTOM_LIB_URL ? MIXPANEL_CUSTOM_LIB_URL : "https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";
                e = f.getElementsByTagName("script")[0];
                e.parentNode.insertBefore(a, e)
            }
        })(document, window.mixpanel || []);

        // default to prod env
        var apiToken = "2c0811ade0d3c40f0f4c7f9193973113";
        if (window.location.hostname.indexOf('staging.solvvy.com') != -1) {
            apiToken = "e5a67bdba580a2e340de16e808e253bc";
        } else if (window.location.hostname.indexOf('test.solvvy.com') != -1) {
            apiToken = "728414011a56b12ef85cb9a079682835";
        } else if (config.debug == true) {
            apiToken = "a8723d67c9633d4874127ab7c4913940";
        }
        mixpanel.init(apiToken);
        mixpanel.register({"clientId": config.id});
    }

    function handleSubmitForm($widget, $form, $results, $input, $search) {
        $form.submit(function (e) {
            e.preventDefault();

            $('.solvvy_load_more', $widget).show();
            $('.solvvy_load_more', $widget).addClass('solvvy_initial_loading');
			$('#most_popular').hide();
            $('.solvvy_results_wrapper', $widget).show();
            $results.empty().show();

            var query = $input.val();
            $input.select();

            mixpanel.track("askQuestion", {"question": query, "from": "WIDGET"});

            if (config.mode == "in-place") {
                $search.attr("disabled", "disabled")
                $search.val("Searching...");
                $('#solvvy_float_widget .tt-dropdown-menu').css('display', 'none');
            } else {
                $('#solvvy_inplace_widget .tt-dropdown-menu').css('display', 'none');
            }
            $.getJSON(config.solvvyApiUrl + "/search/v1/search?q=" + encodeURIComponent(query) + "&key=" + config.id + "&numResults=" + config.numResults + "&startIndex=" + 0 + "&from=WIDGET&types=KB&types=VIDEO&callback=?", function (data) {
                $('.solvvy_load_more', $widget).hide();
                $('.solvvy_load_more', $widget).removeClass('solvvy_initial_loading');
                lastBatchCount = data && data.results ? data.results.length : 0;
                totalResults = lastBatchCount;

                if (config.mode == "in-place") {
                    $search.removeAttr("disabled")
                    $search.val("Search");
                }

                if (!data || !data.results || data.results.length == 0) {
                    $results.append('<div class="solvvy_noresult">' + (config.noResultsText ? config.noResultsText : 'No Results') + '</div>');
                }

                var $mpScripts = $('<script id="solvvy_mp_scripts" type="text/javascript"></script>');
                $results.append($mpScripts);

                if (data.results) {
                    showEntries(totalResults);
                    displayResults(query, data.results, $results, $mpScripts);
                }
                $('#solvvy_float_widget .tt-dropdown-menu').css('display', 'none');
                $('#solvvy_inplace_widget .tt-dropdown-menu').css('display', 'none');
            });
        });
    }

    function appendResult(query, result, i, $results, $mpScripts) {
        //console.log("result: " + JSON.stringify(result));
        if (typeof result.subtitle === "undefined") {
            result.subtitle = "";
        }
        if (typeof result.subtitle === "undefined") {
            result.subtitle = "";
        }
        var badStr = "Last Updated - ";
        if (result.content && result.content.slice(0, badStr.length) === badStr) {
            return;
        }
        var tagsWhitelist = {"span": {"class": ["solvvy_keyword"]}, "br": {}};

        if (overlap(result.title, result.subtitle) > 0.70 || result.title == null || result.title.length == 0) {
            result.title = result.subtitle;
            result.subtitle = '';
        }

        result.title = sanitize(result.title, tagsWhitelist);
        result.subtitle = sanitize(result.subtitle, tagsWhitelist);

        var content = sanitize(result.content, tagsWhitelist);

        var contentLines = content.split(/<br\s*\/?>/);
        // if more than 1 image and more than 1 line of content, treat as stepwise
        var stepwise = result.imageUrls && result.imageUrls.length > 1 && contentLines.length > 1;
        var tmp, stepCount;
        if (stepwise) {
            
            content ='<div class="Stepwise_heading"> Stepwise answer with <span class="solvvy_keyword">' + contentLines.length + '</span> steps  </div>'+ '<ul class="solvvy_steps" style="list-style-type:none">';
            for (var i = 0, len = contentLines.length; i < len; i++) {
                tmp = contentLines[i].split(" ");
                stepCount = +tmp[0]; 
                contentLines[i] = tmp.slice(1).join(" ");
                content += '<li class="solvvy_step"><div class="solvvy_step_content">' +
                 '<span class="solvvy_step_count">' + stepCount + "</span>";
                
                if (result.imageUrls.length > i && result.imageUrls[i]) {
                    content += '<div class="solvvy_step_cont"><img class="solvvy_step_img" id="solvvy_img_side" src="' + result.imageUrls[i] + '" ' +
                            'style="max-width: ' + (config.imageWidth ? config.imageWidth + 'px' : 'inherit') + '" />';
                    content += "<div class='solvvy_step_detail_with_img'>"  + contentLines[i] + '</div></div></div>';
                } else {
                    content += "<div class='solvvy_step_detail'>"  + contentLines[i] + '</div></div>';
                }

                content += '</li>'
            }
            content += '</ul>';
        } else if (result.multipleVideoResults) {
//            content = '<span>' + result.multipleVideoResults.length + ' inner results' + '</span>';
            content = '<div class="solvvy_multiple_video_results" id="multipleVideoResults_' + result.id + '">';
            for(var i=0; i < result.multipleVideoResults.length; i++) {
                var videoResult = result.multipleVideoResults[i];
                if (videoResult.timestamp) {
                    var minute = Math.floor(videoResult.timestamp/60).toString();
                    if (minute.length == 1) {
                        minute = '0' + minute;
                    }
                    var second = (videoResult.timestamp % 60).toString();
                    if (second.length == 1) {
                        second = '0' + second;
                    }
                    videoResult.timestampDisplay = minute + ':' + second;
                    var toggleClass = i > 2 ? ' v_more_result' : '';
                    content +=
                        '<div class="solvvy_inner_video_result'+toggleClass+'"><div class="v-align-center">' +
                            '<div class="solvvy_timestamp_link"><a href="#" data-timestamp="' + videoResult.timestamp + '">' + videoResult.timestampDisplay + '</a></div>' +
                            '<div class="solvvy_result_content">' + sanitize(videoResult.content, tagsWhitelist) + '</div>' +
                        '</div></div>';
                }
            }

            if (result.multipleVideoResults.length > 2) {
                content += '<div id="toggleShowMore_'+ result.id +'" class="toggleShowMore">See more</div>';
            }

            content += '</div>';
        }

        if (result.imageUrls && result.imageUrls.length == 1) {
                result.imageUrl = result.imageUrls[0];
        }
        var isVideo = result.type == "VIDEO";
        
        var displayTitle, displaySubtitle;
        if (config.subtitleOnly) {
            displayTitle = result.subtitle ? result.subtitle : result.title;
            displaySubtitle = '';
        } else {
            displayTitle = result.title;
            displaySubtitle = result.subtitle;
        }

        var totalInnerResult = result.multipleVideoResults && result.multipleVideoResults.length;

        var innerResultText = null;

        if (totalInnerResult > 0) {
            innerResultText = ': <span class="solvvy_keyword">' + totalInnerResult + '</span> inner results';
        }

        var $resultHtml = $(
            '<div class="solvvy_result" data-id="' + result.id + '">' +
                '<div class="solvvy_result-body">' +
                (!isVideo ? '' : '<div class="solvvy_video_title"><span class="solvvy_bold">' + html2text(displayTitle) + '</span>'
                 + (innerResultText ? innerResultText : '') + '</div>') +
               (!config.titlesAbove ? '' :
                '<div id="solvvy_title_' + i + '" class="solvvy_title">' + '<span class="solvvy_found_text">Found in</span>' +
                    (config.titlesLinkable && result.url ?
                        '<a href="' + result.url +'" target="_blank">' + createTitle(result) + '</a>' :
                        createTitle(result)) +
                    (!config.showSrcUrl ? '' :
                        '<div class="solvvy_source">' +
                            (result.url == null ? "" : (result.url.length > 50 ? result.url.substring(0, 50) + "..." : result.url)) +
                        '</div>') +
                '</div>') +
                 (!config.enableVoting ? '' :
                    '<div class="solvvy_feedback ' + (stepwise ? ' sticky_top': '') + '"><div> is this helpful?</div>'+ 
                    '<ul>' +
                    '<li><a href="#" title="" data-vote="Up" class="solvvy_vote-off"><span>yes</span></a></li>' +
                    '<li><a href="#" title="" data-vote="Down" class="solvvy_vote-off"><span>no</span></a></li>' +
                    '</ul>' +
                    '</div>') +
                (result.type != "VIDEO" ? '' :
                        '<div class="result_video" id="resultVideo_'+ result.id +'">' +
                        '<iframe id="yt-embed-' + result.id + '" title="YouTube video player" class="youtube-player" type="text/html" width="640" height="390"' +
                        'src="'+protocol+'www.youtube.com/embed/' + result.externalId + '?enablejsapi=1" frameborder="0" allowFullScreen></iframe>' +
                        '</div>') + 
                '<div class="solvvy_content_container' + (result.type == "VIDEO" ? " solvvy_video_content" : "")  + 
                ' "><div class="solvvy_content" '+
                (result.imageUrls.length != 1 ? '' :
                'id="solvvy_content_img"')+'>' + content + '</div>' +
                (!result.imageUrl ? '' :
                     //   '<div class="solvvy_img" >' +
                        '<img src="' + result.imageUrl + '" ' + 'class="solvvy_img_side"'+
                                'style="max-width: ' + (config.imageWidth ? config.imageWidth + 'px' : 'auto') + '"/>' +
                        '</div>') +
                        //'</div>'+
                '</div>' +
                (config.titlesAbove ? '' :
                '<div id="solvvy_title_' + i + '" class="solvvy_title">' + '<span class="solvvy_found_text">Found in</span>' +
                    (config.titlesLinkable && result.url ?
                            '<a href="' + result.url +'" target="_blank">' + createTitle(result) + '</a>' :
                            createTitle(result)) +
                    (!config.showSrcUrl ? '' :
                        '<div class="solvvy_source">' +
                            (result.url == null ? "" : (result.url.length > 50 ? result.url.substring(0, 50) + "..." : result.url)) +
                        '</div>') +
                '</div>') +
            '</div>');



        var props =
        {
            "referrer": document.referrer,
            "question": query,
            "resultPos": i,
            "resultType": result.type,
            "resultId": result.id,
            "from": "WIDGET"
        };

        $mpScripts.append('mixpanel.track_links("#solvvy_title_' + i + ' a", "clickLink", ' + JSON.stringify(props) + ');');

        $(".solvvy_timestamp_link a", $resultHtml).click(function (e) {
            e.preventDefault();

            var $link = $(this);
            var $result = $link.parents(".solvvy_result");
            var id = $result.attr("data-id");
            var timestamp = $link.attr("data-timestamp");

            function play() {
                $result.data('ytPlayer').seekTo(timestamp);
                $result.data('ytPlayer').playVideo();
            }

            function onPlayerReady(event) {
                play();
            }

            if ($result.data('ytPlayer')) {
                play();
            } else {
                $result.data('ytPlayer', new YT.Player('yt-embed-' + id, {
                    events: { 'onReady': onPlayerReady }
                }));
            }
        });

        $(document).off('click', '.toggleShowMore').on('click','.toggleShowMore', function(e) {
            var targetEle = e.target;
            if (targetEle) {
                var contentId = targetEle.id.split('_')[1];
                var container = document.querySelector('#multipleVideoResults_' + contentId);
                if (container.classList.contains('show_more')) {
                    // already added (showing full results)
                    container.classList.remove('show_more');
                    targetEle.innerHTML = "See more";
                    $('.solvvy_results_wrapper').scrollTo($('#multipleVideoResults_' + contentId));
                } else {
                    container.classList.add('show_more');
                    targetEle.innerHTML = "See less";
                }
            }
            return false;
        });

        $(".solvvy_feedback a", $resultHtml).click(function (e) {
            e.preventDefault();

            var $link = $(this);
            var vote = $link.attr("data-vote");
            var $sib_icon = $("i", $link.parent().siblings().children());
            var $icon = $("i", $link);

            var $result = $link.parents(".solvvy_result");
            var id = $result.attr("data-id");

            $(this).parents('ul').find('a').removeClass('active');
            $(this).addClass('active');

            var undo = false;
            var undoVote = "";
            var lowerCaseVote = vote.toLowerCase();
            var oppVote = vote == "Up" ? "Down" : "Up";
            var lowerOppVote = oppVote.toLowerCase();

            if ($icon.hasClass("fa-thumbs-" + lowerCaseVote)) {
                $icon.removeClass("fa-thumbs-" + lowerCaseVote);
                $icon.addClass("fa-thumbs-o-" + lowerCaseVote);
                undo = true;
                undoVote = vote;
            } else {
                $icon.removeClass("fa-thumbs-o-" + lowerCaseVote);
                $icon.addClass("fa-thumbs-" + lowerCaseVote);
                mixpanel.track("vote" + vote, props);
                sendVote(query, id, vote, result.type, false);
            }
            if ($sib_icon.hasClass("fa-thumbs-" + lowerOppVote)) {
                $sib_icon.removeClass("fa-thumbs-" + lowerOppVote);
                $sib_icon.addClass("fa-thumbs-o-" + lowerOppVote);
                undo = true;
                undoVote = oppVote;
            }
            if (undo) {
                mixpanel.track("undoVote" + undoVote, props);
                sendVote(query, id, undoVote, result.type, true);
            }
        });
        $results.append($resultHtml);
    }

    function createTitle(result) {
        var displayTitle, displaySubtitle;
        if (config.subtitleOnly) {
            displayTitle = result.subtitle ? result.subtitle : result.title;
            displaySubtitle = '';
        } else {
            displayTitle = result.title;
            displaySubtitle = result.subtitle;
        }
        
        var breadCrumb = '';
        if (result.type == 'VIDEO' || result.type == 'TICKET') { 
            breadCrumb = ''
        } else {
            if (displaySubtitle && displaySubtitle.length > 0) {
                breadCrumb = '<li class="solvvy_subtitle_part solvvy_result_src" title="' + html2text(displaySubtitle) + '"><div>' 
                + displaySubtitle + 
                '</div></li>' +
                '</ul>';
            }
        }    
        // (displaySubtitle ? '<span class="solvvy_result_src">&#x300B;</span>' : '') +
        '<li class="solvvy_subtitle_part solvvy_result_src" title="' + html2text(displaySubtitle) + '"><div>' 
        + displaySubtitle + 
        '</div></li>'+'</ul>'
 

        return  '<ul class="breadcrumb"  >' + 
                '<li class="solvvy_title_part solvvy_result_src' 
                + (displaySubtitle ? '' : ' nosubtitle') + '" title="' + html2text(displayTitle) + '"><div>' + 
                displayTitle + 
                '</div></li>' 
                + breadCrumb

        // return '<span class="solvvy_title_part solvvy_result_src' + (displaySubtitle ? '' : ' nosubtitle') + '" title="' + html2text(displayTitle) + '">' + displayTitle + '</span>' +
        //        ((result.type == 'VIDEO' || result.type == 'TICKET') ? '' :
        //         (displaySubtitle ? '<span class="solvvy_result_src">&#x300B;</span>' : '') +
        //         '<span class="solvvy_subtitle_part solvvy_result_src" title="' + html2text(displaySubtitle) + '">' + displaySubtitle + '</span>');
    }

    function html2text(html) {
        return  html ? String(html).replace(/<[^>]+>/gm, '') : '';
    }


    function toggleAndVote(icon, sibIcon, vote) {
        var undo = false;

        if (icon.hasClass("fa-thumbs-" + lowerCaseVote)) {
            icon.removeClass("fa-thumbs-" + lowerCaseVote);
            icon.addClass("fa-thumbs-o-" + lowerCaseVote);
            undo = true;
        } else {
            icon.removeClass("fa-thumbs-o-up" + lowerCaseVote);
            icon.addClass("fa-thumbs-up" + lowerCaseVote);
            mixpanel.track("voteUp", props);
            sendVote(query, id, vote, result.type, false);
        }
        if (sib_icon.hasClass("fa-thumbs-down" + lowerCaseVote)) {
            sib_icon.removeClass("fa-thumbs-down" + lowerCaseVote);
            sib_icon.addClass("fa-thumbs-o-down" + lowerCaseVote);
            undo = true;
            undoVote = "Down";
        }
        if (undo) {
            mixpanel.track("undoVote" + undoVote, props);
            sendVote(query, id, vote, result.type, true);
        }
    }

    function displayResults(query, results, $results, $mpScripts) {
        var displayResults = [];
        var videos = {};
        var ytApiInitialized = false;
        $.each(results, function (i, result) {
            var showResult = true;
            if (result.type == 'VIDEO') {

                // init YT JS API
                if (!ytApiInitialized) {
                    var tag = document.createElement('script');
                    tag.src = protocol+"www.youtube.com/iframe_api";
                    var firstScriptTag = document.getElementsByTagName('script')[0];
                    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                }

                var video = videos[result.url];
                if (video) {
                    // existing video URL
                    if (!video.multipleVideoResults) {
                        video.multipleVideoResults = [ $.extend({}, video) ];
                    }

                    video.multipleVideoResults.push(result);
                    showResult = false;
                } else {
                    videos[result.url] = result;
                }
            }

            if (showResult) {
                displayResults.push(result);
            }
        });

        $.each(displayResults, function (i, result) {
            if (result.multipleVideoResults) {
                result.multipleVideoResults = result.multipleVideoResults.sort(function(a,b) {
                    return a.timestamp - b.timestamp;
                });
            }
            appendResult(query, result, i, $results, $mpScripts);
        });
    }

    function handleScroll($widget, $results, $input) {
        if (config.scrollForMoreResults) {
            var loadingMoreResults = false;
            var scrollContainer = config.scrollContainer == "window" ? window : config.scrollContainer;
            $(scrollContainer).scroll(function() {
                if (lastBatchCount > 0 && !loadingMoreResults) {
                    var scrollTop = $(scrollContainer).scrollTop();
                    var contentHeight = config.scrollContainer == "window" ? $(document).height() : $(scrollContainer)[0].scrollHeight;
                    var viewHeight = $(scrollContainer).height();
//                    console.log("scroll event, scrollTop", scrollTop, "CH", contentHeight, "VH", viewHeight, "DH - WH", contentHeight - viewHeight);

                    if(scrollTop >= contentHeight - viewHeight) {
                        $('.solvvy_load_more', $widget).show();

                        var query = $input.val();
                        var $mpScripts = $("#solvvy_mp_scripts", $results);

                        var startIndex = totalResults;
                        loadingMoreResults = true;

                        $.getJSON(config.solvvyApiUrl + "/search/v1/search?q=" + encodeURIComponent(query) + "&key=" + config.id + "&numResults=" + config.numResults + "&startIndex=" + startIndex + "&from=WIDGET&types=KB&types=VIDEO&callback=?", function (data) {
                            loadingMoreResults = false;
                            $('.solvvy_load_more', $widget).hide();
                            lastBatchCount = data && data.results ? data.results.length : 0;
                            totalResults += lastBatchCount;
                            showEntries(totalResults);

                            displayResults(query, data.results, $results, $mpScripts);
                        });
                    }
                }
            });
        }
    }

    function searchBoxQueryChanged() {
        var search_img = document.getElementById('search_solvvy_img');

        var $input = $("input[name=solvvy_q]");
        
        if ($input.val().length > 0) {
            $('#reset_search_box').show();
            $('#solvvy_input_search').css({ "font-weight": "normal" });
            search_img.src="search_icon_bold.png";
            $('#solvvy_header').html('You can use commas to search several terms at once');
            // $('#solvvy_float_widget form').removeClass('solvvy_results_onshow');
        } else {
            $('#reset_search_box').hide();
            search_img.src="search_icon.png";
            $('#solvvy_header').html('Looking for something?');
            // $('#solvvy_float_widget form').removeClass('solvvy_results_onshow');
        }
        $('#solvvy_results_count').html('');
        $('#solvvy_input_search').width('100%');
    }

    function initQuerySuggestions($form, $input) {
        if (config.querySuggestions) {
            // constructs the suggestion engines
            var suggestTypes = "page";
            if (config.userQueriesAsSuggestions) {
                suggestTypes += ",query";
            }
            var questionSuggestions = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace("text"),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: config.solvvyApiUrl + "/search/v1/suggest?q=%QUERY&types=" + suggestTypes + "&key=" + config.id,
                    rateLimitWait: 0
                    //ajax: {
                    //    success: function () {
                    //        $results.show();
                    //    }
                    //}
                }
            });

            questionSuggestions.initialize();

            $("#reset_search_box").click(function(e) {
                var search_img = document.getElementById('search_solvvy_img');
                $('#reset_search_box').hide();
                search_img.src="search_icon.png";
                $('#solvvy_header').html('Looking for something?');
                $('#solvvy_float_widget form').removeClass('solvvy_results_onshow');
                $('#solvvy_results_count').html('');
                $input.focus();
            });
            
            $("#solvvy_input_search").keyup(searchBoxQueryChanged);

            $input.typeahead({
                        minLength: 1,
                        hint: false,
                        highlight: false
                    }, {
                        name: "question-suggestions",
                        displayKey: "text",
                        maxItem: 3,
                        source: questionSuggestions.ttAdapter(),
                        templates: {
                            suggestion: function (obj) {
                                return obj.html;
                            }
                        }
                    }
            );
            $input.on("typeahead:selected", function (e, suggestion, data) {
                $form.submit();
                $('.tt-dropdown-menu').css('display', 'none');
            });
        }
    }


    function initInPlaceSearchWidget() {
        var $widget = $("#solvvy_inplace_widget");
        var $form = $("form", $widget);
        var $input = $("input[name=solvvy_q]", $form);
        var $results = $(".solvvy_results", $widget);
        var $search = $(".solvvy_search", $form);

        initQuerySuggestions($form, $input);
        handleSubmitForm($widget, $form, $results, $input, $search)
        handleScroll($widget, $results, $input);
    }


    function initFloatingSearchWidget() {
        var $button = $('#solvvy_button');
        var $widget = $("#solvvy_float_widget");
        var $collapse = $(".solvvy_collapse", $widget);
        var $header = $(".solvvy_header", $widget);
        var $expand = $(".solvvy_expand", $widget);
        var $form = $("form", $widget);
        var $input = $("input[name=solvvy_q]", $form);
        var $results = $(".solvvy_results", $widget);
        var $results_wrapper = $(".solvvy_results_wrapper", $widget);
        var $footer = $(".solvvy_footer", $widget);


        var $queriesHtml = $('<div class="solvvy_popular_queries"></div>');

        if (config.contactUsUrl) {
            var $contactUsLink = $('<div class="solvvy_contact-container"><a href="' + config.contactUsUrl + '" class="solvvy_contact-link" style="text-decoration: none">Contact us</a></div>');
            $footer.append($contactUsLink);
        }

        if (config.chatUrl) {
            var $chatLink = $('<div class="solvvy_contact-container"><a href="' + config.chatUrl + '" class="solvvy_contact-link" style="text-decoration: none">Live chat</a></div>');
            $footer.append($chatLink);
        }

        var loadedPopular = false;
        $input.focus(function () {
            $widget.addClass("solvvy_expanded");
            $expand.hide();
            $collapse.show();
            $footer.show();
            $results.show();
            $results_wrapper.show();

            if (!loadedPopular) {
                loadedPopular = true;
                $.getJSON(config.solvvyApiUrl + "/search/v1/queries?key=" + config.id + "&callback=?", function (data) {

                    $.each(data, function (i, query) {
                        var $queryHtml = $('<a class="solvvy_query" href="javascript:void(0)"><div>' + query + '</div></a>');
                        $queryHtml.click(function () {
                            $input.val(query);
                            $input.focus();
                            $form.submit();
                            searchBoxQueryChanged();
                        });
                        $queriesHtml.append($queryHtml);
                    });
                    $results.append($queriesHtml);
                });
            }
        });
        $input.focus();
        initQuerySuggestions($form, $input);
        handleSubmitForm($widget, $form, $results, $input);
        handleScroll($widget, $results, $input);

        $(".solvvy_collapse", $widget).click(function (e) {
            e.preventDefault();

            $widget.removeClass("solvvy_expanded");
            $collapse.hide();
            $results.hide();
            $footer.hide();
            if ($results.children().length > 0) {
                $expand.show();
            }
        });
        $(".solvvy_expand", $widget).click(function expand(e) {
            e.preventDefault();

            $widget.addClass("solvvy_expanded");
            $header.addClass("solvvy_expanded_header");
            $expand.hide();
            $collapse.show();
            $results.show();
            $footer.show();
        });

        $($button).click(function() {
          $('#solvvy_button').hide();
          $('body').append('<div class="solvvy_modal_overlay"><div>');
          $('#solvvy_left').addClass('magictime slideRight');
          $('#solvvy_input_search').focus();
        });
        $('#left_arrow', $widget).click(function() {
             var s_close = document.getElementById('left_arrow');
             var image = document.getElementsByClassName('solvvy_widget_icon');
             $("#solvvy_left").removeClass('slideRight');
             $("#solvvy_left").addClass('slideRightRetourn');
             $('#solvvy_left').one("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd",
                 function(e){
                    $('.solvvy_modal_overlay').fadeOut(500, function() {
                      $('#solvvy_left').removeClass('magictime slideRightRetourn');
                      s_close.src = "left_arrow.png";
                      $('#solvvy_button').show();
                      $('.solvvy_modal_overlay').remove();
                    });
                    $(this).off("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd");
                 });
        });
    }

    function sendVote(query, id, vote, type, undo) {
        var params = {
            q: query,
            solutionId: id,
            vote: vote,
            key: config.id,
            from: "WIDGET",
            type: type,
            callback: "?",
            undo: undo
        };
        $.getJSON(config.solvvyApiUrl + "/search/v1/vote", params);
    }

    function minJQueryVersion(version) {
        var $vrs = window.jQuery.fn.jquery.split('.'),
            min = version.split('.');
        for (var i = 0, len = $vrs.length; i < len; i++) {
            var v = (+$vrs[i]);
            if (min[i] && (isNaN(v) || v < (+min[i]))) {
                return false;
            }
        }
        return true;
    }

    function initJQuery(callback) {
        //if the jQuery object isn't available
        if (!window.solvvyJQuery && (typeof(window.jQuery) == 'undefined' || !minJQueryVersion("1.9"))) {

            if (!jQueryScriptOutputted) {
                //only output the script once..
                jQueryScriptOutputted = true;

                //output the script (load it from google api)
                document.write(
                    '<scr' + 'ipt type="text/javascript" src="' + config.jQueryUrl + '"></scr' + 'ipt>' +
                    '<scr' + 'ipt type="text/javascript">window.solvvyJQuery = window.jQuery.noConflict(true);</script>');
            }
            setTimeout(function () {
                initJQuery(callback);
            }, 50);
        } else {
            var myJQuery = window.solvvyJQuery || window.jQuery;
            myJQuery(document).ready(function ($) {
                callback($);
            });
        }
    }

    function overlap(s1, s2) {
        var len = longestCommonSubstring(s1 || '', s2 || '');
        var olap1 = len / (s1 || '').length;
        var olap2 = len / (s2 || '').length;
        var ret = (olap1 + olap2) / 2;
        return ret;
    }

    //http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring#JavaScript
    function longestCommonSubstring(string1, string2) {
        // init max value
        var longestCommonSubstring = 0;
        // init 2D array with 0
        var table = [],
            len1 = string1.length,
            len2 = string2.length,
            row, col;
        for (row = 0; row <= len1; row++) {
            table[row] = [];
            for (col = 0; col <= len2; col++) {
                table[row][col] = 0;
            }
        }
        // fill table
        var i, j;
        for (i = 0; i < len1; i++) {
            for (j = 0; j < len2; j++) {
                if (string1[i] == string2[j]) {
                    if (table[i][j] == 0) {
                        table[i + 1][j + 1] = 1;
                    } else {
                        table[i + 1][j + 1] = table[i][j] + 1;
                    }
                    if (table[i + 1][j + 1] > longestCommonSubstring) {
                        longestCommonSubstring = table[i + 1][j + 1];
                    }
                } else {
                    table[i + 1][j + 1] = 0;
                }
            }
        }
        return longestCommonSubstring;
    }

    function trimAttributes(node, allowedAttrs) {
        var passed = true;
        $.each(node.attributes, function () {
            var attrName = this.name;
            var val = this.value;

            if ($.inArray(attrName, Object.keys(allowedAttrs)) == -1 || $.inArray(val, allowedAttrs[attrName]) == -1) {
                $(node).removeAttr(attrName);
                passed = false;
            }
        });
        return passed;
    }

    // https://gist.github.com/kaznum/3810258
    function sanitize(html, whitelist) {
        if (!html) {
            return html;
        }
        var output = $('<div class="sanitized">' + html + '</div>');
        output.find('*').each(function () {
            var allowedAttrs = whitelist[this.nodeName.toLowerCase()];
            if (!allowedAttrs) {
                $(this).remove();
            } else {
                var passed = trimAttributes(this, allowedAttrs);
                if (!passed) {
                    $(this).remove();
                }
            }
        });
        return output.html();
    }

    function showEntries(num) {
      // $('#solvvy_float_widget form').addClass('solvvy_results_onshow');
      $('#solvvy_header').html('Results for');
      var txt = num <= 1 ? ' entry found' : ' entries found';
      $('#solvvy_results_count').html(num + txt);
      $('#solvvy_input_search').width('80%');
    }

    return {
        createSearchWidget: createSearchWidget
    };

})();
